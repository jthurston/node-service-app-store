var express = require('express');
var app = express();

app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

var theapps = [
    { appname : 'Beauxsacs', desc : "Beauxsacs Mobile Version", apptype: "Web", appicon: "beauxsacs.png", androidlink:"", ioslink:"", weblink:"http://www.beauxsacs.net", appinfolink: "http://www.beauxsacs.net", version:"1.0"},
    { appname : 'Code Creator', desc : "X10 and Insteon code generator", apptype: "Web", appicon: "cc.png", androidlink:"", ioslink:"", weblink:"", appinfolink: "", version:"1.0"},
    { appname : 'Gadget Decal Vinyl Estimator', desc : "Gadget Decal's tool to determine the cost of a custom vinyl decal based on size and job work. (Web)", apptype: "Web", appicon: "vinylcalc.png", androidlink:"", ioslink:"", weblink:"http://www.gadgetdecal.com/estimate/", appinfolink: "http://www.thistymandesings.com/apps/vinycalc", version:"1.0"},
    { appname : 'SOWO', desc : "Southern Worthersee's 2014 event app (iTunes)", apptype: "Public", appicon: "sowo.png", androidlink:"", ioslink:"https://itunes.apple.com/us/app/sowo/id804477364?ls=1&mt=8", weblink:"", appinfolink: "", version:"1.0"},
    { appname : 'Space365', desc : "A NASA space and world history time line.", apptype: "Public", appicon: "space365.png", androidlink:"", ioslink:"", weblink:"", appinfolink: "", version:"1.0"}
];

app.use(express.bodyParser());

app.get('/', function(req, res) {
  res.json(theapps);
});

app.get('/theapp/random', function(req, res) {
  var id = Math.floor(Math.random() * theapps.length);
  var q = theapps[id];
  res.json(q);
});

app.get('/theapp/:id', function(req, res) {
  if(theapps.length <= req.params.id || req.params.id < 0) {
    res.statusCode = 404;
    return res.send('Error 404: No theapp found');
  }

  var q = theapps[req.params.id];
  res.json(q);
});

app.post('/theapp', function(req, res) {
  if(!req.body.hasOwnProperty('appname') || !req.body.hasOwnProperty('desc')) {
    res.statusCode = 400;
    return res.send('Error 400: Post syntax incorrect.');
  }

  var newtheapp = {
    appname : req.body.appname,
    desc : req.body.desc
  };

  quotes.push(newtheapp);
  res.json(true);
});

app.delete('/theapp/:id', function(req, res) {
  if(theapps.length <= req.params.id) {
    res.statusCode = 404;
    return res.send('Error 404: No theapp found');
  }

  theapps.splice(req.params.id, 1);
  res.json(true);
});

app.listen(process.env.PORT || 3412);
console.log ("Listening on Port 3412");